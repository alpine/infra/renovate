ARG ALPINE_VERSION=3.21
# renovate: datasource=repology depName=node lookupName=alpine_3_21/nodejs extractVersion=(?<version>\d+)
ARG NODE_VERSION=22
# renovate: datasource=npm depName=renovate
ARG RENOVATE_VERSION=39.176.0
FROM registry.alpinelinux.org/alpine/infra/docker/golang:latest as golang
# renovate: datasource=github-releases depName=marwan-at-work/mod
ENV MOD_VERSION=v0.7.1
RUN go install github.com/marwan-at-work/mod/cmd/mod@${MOD_VERSION}

FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION} as nodejs
USER node
RUN npm install -g --prefix /home/node/renovate renovate@${RENOVATE_VERSION}

FROM alpine:${ALPINE_VERSION}

COPY overlay /
RUN <<EOF
    set -eu
    apk add --no-cache nodejs go git su-exec curl make
    adduser -D renovate
    install -dm0755 -o renovate -g renovate /home/renovate/cache
    install -dm0755 -o renovate -g renovate /home/renovate/repos
    ln -s /usr/local/bin/run-renovate /etc/periodic/hourly/renovate
EOF

COPY --from=golang --chmod=0755 /home/build/go/bin/mod /usr/local/bin/mod
COPY --from=nodejs /home/node/renovate /usr/local/
COPY config.json /home/renovate/config.json

ENV RENOVATE_BASE_DIR=/home/renovate

VOLUME ["/home/renovate/cache", "/home/renovate/repos"]

WORKDIR /home/renovate

ENTRYPOINT ["/usr/local/bin/entrypoint"]
